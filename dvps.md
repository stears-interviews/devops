# Stears Coding Homework

Thanks for your application to Stears

This interview is intended for a devops engineer with experience in cloud tools, containerisation, container orchestration and ci/cd. We expect that you demonstrate skills in the above tools by completing this exercise.

Either complete all tasks or attempt as much as possible so we have enough information to assess your skill & experience.

#### Submissions

- Create a private [gitlab](https://gitlab.com/) repository and add @foluso_ogunlana as a maintainer
- Create a README.md with the following:
  - Answers to the questions here with code samples
  - Clear setup instructions if necessary

#### Deadlines

You will be given at least 5 working days to complete the task and your deadline will be communicated via email.

#### Assessment

Our favourite submissions are -

1. **Rigorous** - Complete solutions with well researched applications of the right technology choices.
2. **Simple** - Solve the problem in a minimal manner and build on your solution incrementally.
3. **Robust** - Propose solutions that are reliable and will not break on edge cases.

Your submission will also impress us by demonstrating one or more of the following -

- **Mastery** in your general use of language, libraries, frameworks, architecture / design / deployment patterns
- **Unique skills** in any specific areas of the devops process

#### Conduct

By submitting, you assure us that you have not shared the test with anyone else and that all work submitted is your own. Though you are allowed to use whatever online or offline resources you see fit to learn skills for the tasks.

# **Coding Homework**

## Start here

Follow the story below. You're on a journey with an imaginary company - "The Shing Inc." as they learn to scale their web application "Mandly". There isn't always a "right" answer for each question, but the most obvious answer is usually best.

### Task 1 - de-risking code execution

The Shing Inc. team is a single developer and she has a server running a python web app worker and async worker process to handle background tasks or long running cron jobs. The web worker is run with `python ~/scripts/mandly-web.py` in local development and `sudo systemctl start shing.service (ubuntu’s systemctl)` on their server. The cron jobs run a different script - “python ~/scripts/mandly-worker.py” every day at 1am.

To update the code running on the server, the team meets and commits directly to “master” branch, pushes it to the remote. When the product manager is ready to ship the latest features, a member of the team SSHs into the server runs a `cd ~/scripts && git pull && sudo systemctl restart mandly.service` to get the latest code in `~/scripts/` and to restart `python ~/scripts/mandly-web.py`. The team is frustrated however, because they often forget which python dependencies to install on each latest deployment. They've deployed late several times and disappointed their customers. They are running out of time and need a quick fix to improve the reliability of their deployments. 

Can you help them make a repeatable process for running their application with all the right dependencies installed, without too much hassle?

#### Definition of done

- [ ] Solution proposed for repeatable process and tools to use for having right dependencies installed before each deployment
- [ ] Solution proposed should be repeatable on any server with a minimal set of initial dependencies (which you can specify)
- [ ] Solution code samples included in repo, along with a guide in the README.md for how it should / would be executed
- [ ] Solution is repeatable, and easy to update and extend

### Task 2 - de-risking code integration

The Shing Inc. has scaled from a team of 1 to a team of 2. Both developers make commits directly to the master branch (where they deploy from using their new deployment tools). However, they find that the master branch has a lot of bugs because they both worked on the same files at the same time. There are some unit tests in the code repository but no one runs them before committing their work to master. So a lot of the tests do not pass. 

Can you propose a workflow and set of tools that ensures the master branch is always in a valid state and ready to be deployed?

#### Definition of done

- [ ] Solution proposed protects the master branch by ensuring tests will always pass on it
- [ ] Solution proposed ensures that there will always be a deployable artefact generated on the master branch (even if the team still SSHs into the server to deploy the artefact)
- [ ] Solution code samples included in repo, along with a guide in the README.md for how it should / would be executed
- [ ] Solution is repeatable and easy to update and extend

### Task 3 - de-risking deployments

Thanks to you, The Shing Inc. only releases reliable code that has been well integrated beforehand. Thanks to your hard work, they are now experiencing growing pains and are willing to invest a lot of time and effort in solving some of their on-going problems.

1. Their customers are growing now and so is the demand on the Mandly web application and worker. They've manually upgraded their server and tweaked the number of processes being used several times to up scale (and down scale) their web requests and more CPU & memory intensive long running jobs. 
2. In addition, the management is pushing back on the development team. Mandly has a lot of features and bug fixes so the team wants to deploy small changes often, but the management wants larger and less frequent deployments because each deployment leads to a minimal downtime which affects their customer experience and revenue.
3. Their releases process often fails due to human error, as the team is manually SSHing into the server to re-run the application whenever they want to release a new set of artefacts
4. The site seems to run out of resources and stop working (Internal Server Error) randomly once every 3 months or so because of an unknown bug. It is usually fixed by re-running the site. Without finding the bug and fixing it, they would like to have the site automatically heal by having it restart without supervision.

Can you propose a solution that allows Mandly to run its web worker, background tasks and long running jobs in a single self-contained system that is server and application agnostic and can scale in the long run?

#### Definition of done

- [ ] Solution proposed allows Mandly to be deployed in a repeatable manner without too much opportunity for human error
- [ ] Solution proposed should restore trust between the management and development team so they are both happy
- [ ] Solution proposed has clear potential to scale the application's resources both vertically and horizontally if required
- [ ] Solution proposed keeps the site healthy by monitoring it and restarting it automatically
- [ ] Solution code samples included in repo, along with a guide in the README.md for how it should / would be executed
- [ ] Solution is easy to update and extend


### Task 4 - proactive monitoring

The Shing Inc. is a huge success after following your advice, but they still have bugs and issues coming up from time to time causing the site to go down for maintenance or for major bug fixes. They now have a Service Lifetime Objective of keeping the site responsive to within 200ms for 95% of requests over a rolling period of any 3 months. They also promise their customer that they will proactively spot any errors in the system and take action within 3 days, even on the weekends.

Can you propose a set of tools and practices that can be used to achieve these Service Level Objectives set out by the team?

#### Definition of done

- [ ] Solution proposed should allow the team monitor their performance agains the SLO at all times
- [ ] Solution proposed should ensure someone in the team is aware of, and is able to immediately respond to any downtime (or sign of poor application health) within 1 minute of it's occurrence
- [ ] Solution proposed should ensure someone in the team is immediately notified of any non-critical error that occurs in the site that needs a response within 3 days
- [ ] Solution code samples included in repo, along with a guide in the README.md for how it should / would be executed
- [ ] Solution is easy to update and extend


### (Final) Task 5 - identity and access management 

The Shing Inc. now has 30 employees - 10 of which are developers. The CEO still controls the cloud access keys and the API keys to all the services they use for security purposes or because she doesn't know who to trust with this sensitive information. This is slowing down the team because the developers need the keys to update applications and the CEO is busy so takes a while to figure out what access level is needed for each request and then resolve it.

Can you propose a centralised system for managing access to the keys that the CEO can trust to empower the developers of varying responsibilities without giving them all of them access to everything?

#### Definition of done

- [ ] Solution proposed should let the CEO have access to everything if needed
- [ ] Solution proposed should only allow developers access whatever they need when they need it if appropriate
- [ ] Solution proposed should should be easy to manage for all parties involved and should make use of their existing tools (e.g. git)
- [ ] Solution code samples included in repo, along with a guide in the README.md for how it should / would be executed
- [ ] Solution is easy to update and extend

## Thanks!

If you made it this far, thanks for your time.
We look forward to reviewing your solid application with you!
